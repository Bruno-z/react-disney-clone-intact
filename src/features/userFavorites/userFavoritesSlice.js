import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  movies: []
};

const userFavoritesSlice = createSlice({
  name: "userFavorites",
  initialState,
  reducers: {
    userFavoritesUpdated: (state, action) => {
      state.movies = action.payload.movies;
      console.log('###### userFAvoritesSlice')
    },
  },
});

export const { userFavoritesUpdated } = userFavoritesSlice.actions;

export const selectUserName = (state) => state.user.name;
export const selectUserEmail = (state) => state.user.email;
export const selectUserPhoto = (state) => state.user.photo;

export default userFavoritesSlice.reducer;
