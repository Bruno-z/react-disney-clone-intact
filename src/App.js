import React from 'react';
import { Counter } from './features/counter/Counter';
import ReactDOM from 'react-dom';
import { Navigate } from 'react-router-dom';
import './App.css';
import { Outlet } from 'react-router-dom';
import Header from './components/Header';
import Home from './components/Home';
import Detail from './components/Detail';
import { useEffect, useState } from "react";
import { ProtectedRoute } from './components/ProtectedRoute';
import Login from './components/Login';
import { AuthContext } from './firebaseAuthContext';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { provider } from './firebase'
import MaListe from './components/MaListe';


// function PrivateOutlet() {
//   const auth = useAuth();
//   return auth ? <Outlet /> : <Navigate to="/" />;
// }
// function useAuth() {
//   return AuthContext;
// }


function App() {
  return (
    <div className="App">
      <Router>
        <Header />
        <Routes>
          <Route path="/" element={<Login />} />
          {/* <Route path="/PrivateOutlet " element={<Login />}/> */}
          {/* <Route path ='/' element={<ProtectedRoute/>} /> */}
          <Route path='/home' element={
            <ProtectedRoute>
              <Home />
            </ProtectedRoute>} />
          <Route path='/detail/:id' element=
            {<ProtectedRoute>
              <Detail />
            </ProtectedRoute>
            } />
          <Route path='/maListe' element={
            <ProtectedRoute>
              <MaListe />
            </ProtectedRoute>
          } />
        </Routes>
      </Router>


    </div>
  );
}

export default App;
