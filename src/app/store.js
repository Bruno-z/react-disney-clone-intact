import { configureStore, getDefaultMiddleware  } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import movieReducer from '../features/movie/movieSlice';
import userReducer from "../features/user/userSlice"
import userFavoritesReducer from "../features/userFavorites/userFavoritesSlice"


export const store = configureStore({
  reducer: {
    movie: movieReducer,
    user: userReducer,
    userFavorites: userFavoritesReducer,
  },
});
