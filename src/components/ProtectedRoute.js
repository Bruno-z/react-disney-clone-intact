import React, { Children, useContext } from 'react';
import { Navigate, Route } from 'react-router-dom';
import { AuthContext, AuthProvider } from '../firebaseAuthContext';
import db, { auth, provider } from '../firebase';
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import {
    selectUser
  } from '../features/user/userSlice';
  


const ProtectedRoute = ({children}) => {
    //  auth.onAuthStateChanged(async (user) => {
     const user = useSelector(selectUser);
    console.log("TEST USER 666666666666666666666",user);

        if (user.email==null ||user.email==''){
            return <Navigate to ='/'/>;
        }
    return children;
};
export    {ProtectedRoute}; 