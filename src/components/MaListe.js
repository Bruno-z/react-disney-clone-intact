import React, { Component } from 'react';
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import styled from "styled-components";
import firestore from 'firebase';


import db from "../firebase";
import ReactPlayer from 'react-player'
import VideoTrailer from './VideoTrailer';
import Youtube from "react-youtube"
import movieTrailer from 'movie-trailer';
import { useDispatch, useSelector } from "react-redux";

import {
  selectUser
} from '../features/user/userSlice';
import Detail from './Detail';


const MaListe = (props) => {
  const { id } = useParams();
  const [detailData, setDetailData] = useState({});
  const [trailerPlaying, setTrailerPlaying] = useState(false);
  const [backGroundChange, setBackGroundChange] = useState(false);
  const [trailerURL, setTrailerUrl] = useState("");
  const user = useSelector(selectUser);
  const [favoritesList, setFavoriteList] = useState([]);
  const userFavorites = useSelector(state => state.userFavorites);



  useEffect(async () => {
    const ufDoc = await db.collection('userFavorites').doc(user.uid);
    const ufSnapShot = await ufDoc.get();
    const ufMovies = ufSnapShot.data()?.movies;
    console.log("#")
    const moviesSnapshot = await db.collection("movies").get();
    const myFavoriteArrMovie = [];
    moviesSnapshot.forEach(doc => {
      const movie = doc.data();

      if (movie && ufMovies.includes(doc.id)) {
        myFavoriteArrMovie.push(movie);
      }

    });

    setFavoriteList(myFavoriteArrMovie);

  }, [id]);



  // function DeleteMovieFromList(movieId) {
  //   if (user && user.uid) {
  //     console.log(JSON.stringify(userFavorites))
  //   var favoriteRef=  db.collection('userFavorites')
  //       .doc(user.uid).set({movies: userFavorites.movies.concat([movieId])})

  //     let docRef= this.db.doc('userFavorites');
  //     let removeList = docRef.update({
  //       [movieId] :db.fieldValue.delete()
  //     });
  //     console.log('test delete'+ [movieId]);

  //   }

  // }




  return (
    <Container>


      <Background>
        <img src={backGroundChange} />
      </Background>


      <ImageTitle>
        <hr>
        </hr>
      </ImageTitle>



      <DetailList>
        <h2>"Art challenges technology, technology inspires the art"</h2>

        <NavMenu>

          <a>
            <span>RECOMENDED</span>
          </a>
          <a>
            <span>DETAILS</span>
          </a>
          <a>
            <span>BONUS</span>
          </a>

        </NavMenu>

      </DetailList>


      <Gallery>
        {
          favoritesList.map(m => (<img onClick={() => {
            setBackGroundChange(m.background);
            //  setFavoriteList(m.titleImg)
          }

          } src={m.backgroundImg}></img>))

        }


      </Gallery>
    </Container>



  );

};

const MyVideo = styled.div`
position: absolute;
top:50px;
bottom:0;
margin-left:35%;
height:80%;
width: 55%;
* @media (max-width: 768px) {
  display: none;
  bottom:0
  position:absolute;
  top: 200px;

} */
`

const NavMenu = styled.div`
  cursor: grab;

  align-items: center;
  display: flex;
  flex-flow: row nowrap;
  height: 100%;
  justify-content: flex-start;
  margin: 0px;
  padding: 0px;
  margin-top:100px;
  position: relative;
  margin-right: auto;
  margin-left: 25px;
  a {
    display: flex;
    align-items: center;
    padding: 0 12px;
    img {
      height: 20px;
      min-width: 20px;
      width: 20px;
      z-index: auto;
    }
    span {
      color: rgb(249, 249, 249);
      //  overflow:hidden;
      font-size: 13px;
      letter-spacing: 1.42px;
      line-height: 1.08;
      padding: 2px 0px;
      white-space: nowrap;
      position: relative;
      &:before {
        background-color: rgb(249, 249, 249);
        
        border-radius: 0px 0px 4px 4px;
        bottom: -6px;
        content: "";
        height: 2px;
        left: 0px;
        opacity: 0;
        position: absolute;
        right: 0px;
        transform-origin: left center;
        transform: scaleX(0);
        transition: all 250ms cubic-bezier(0.25, 0.46, 0.45, 0.94) 0s;
        // visibility: hidden;

        width: auto;
      }
    }
    &:hover {
      span:before {
        transform: scaleX(1);
        visibility: visible;
        opacity: 1 !important;
        color:red;
      }
    }
  }
  /* @media (max-width: 768px) {
    display: none;
  } */
  `;

const DetailList = styled.div`
margin-top: 40px;
text-align :center;
font-size: 15px;



`
const Container = styled.div`

  min-height: calc(100vh-250px);
  overflow-x: hidden;
  display: block;
  top: 42px;

  padding: 0 calc(3.5vw + 5px);
`;

const Gallery = styled.div`  
margin-bottom : 150px;


/* Valeur de masque */
top:29%;
position:absolute;
cursor:grab;  
display: grid;
margin-top:100px;
  grid-template-rows: repeat(5, 18vh);
  grid-template-columns: repeat(2, 45vw);
  transition: all 250ms cubic-bezier(0.25, 0.46, 0.45, 0.94) 0s;
  background: rgba(0, 0, 0, 0.9);
  border-radius: 10px;
  padding: 0.25em;
  cursor: zoom-in;
   grid-gap: .45em;

  @media (min-width: 40em) {
    grid-template-rows: repeat(2, 20vh);
  }

  @media (min-width: 10em) and (orientation: landscape) {
    grid-template-columns: repeat(5, 18vw);
    grid-template-rows: repeat(2, 45vh);
  }
    
 @media (min-width: 60em) {
    grid-template-columns: repeat(10, 8vw);
    grid-template-rows: 25vh;
  }
}

img {

  -webkit-box-reflect: below 2px linear-gradient(transparent, rgba(0, 0, 0, .5));
  cursor:grab;  

  object-fit: cover;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  transition: all $time/2 ease(downhill);
  position: relative;
  box-shadow: 0 0 0 #0000;
  opacity: 0.67;
  // filter: sepia(80%) hue-rotate(180deg);

  &:first-child {
    border-radius: 10px 10px 0 0;
  }
  &:last-child {
    border-radius: 0 0 10px 10px;
  }

  @media (min-width: 40em) and (max-width: 59.99em) {
    &:first-child {
      border-radius: 10px 0 0 0;
    }
    &:nth-child(5) {
      border-radius: 0 10px 0 0;
    }
    &:nth-child(6) {
      border-radius: 0 0 10px 0;
    }
    &:last-child {
      border-radius: 0 0 0 10px;
    }
  }

  @media (min-width: 60em) {
    &:first-child {
      border-radius: 10px 0 0 10px;
    }
    &:last-child {
      border-radius: 0 10px 10px 0;
    }
  }

  &:hover {
    opacity: 1;
    z-index: 1;
    box-shadow: 1em 1em 1em #0004;
    box-shadow: rgb(0 0 0 / 80%) 0px 40px 58px -16px,
      rgb(0 0 0 / 72%) 0px 30px 22px -10px;
    transform: scale(1.05);
    // filter: sepia(0%) hue-rotate(0deg);
    border-radius: 5px;
    width: 120%;
    height: 110%;
    left: -100%;
    top: -100%;

    @media (min-width: 40em) {
      width: 150%;
      height: 400%;
      left: -75%;
      top: -200%;
    }
    
    @media (min-width: 10em) and (orientation: landscape) {
      width: 120%;
      height: 110%;
    }
       
    @media (min-width: 40em) and (orientation: portrait) {
      width: 120%;
      height: 110%;
      left: -100%;
      top: -100%;
    }

    @media (min-width: 60em) {
       width: 120%;
      height: 110%;
      left: -75%;
      top: -25%;

      ~ img {
        left: 175%;
      }
    }

    @media (min-width: 60em) and (orientation: landscape) {
      width: 300%;
      height: 300%;
      left: -75%;
      top: -100%;

      ~ img {
        left: 100%;
      }
    }
  }
}
`;


const Background = styled.div`
background: url("https://c4.wallpaperflare.com/wallpaper/673/107/786/up-movie-pixar-animation-studios-movies-sky-wallpaper-preview.jpg") center center / cover;

left: 0px;
  opacity: 0.8;
  position: fixed;
  right: 0px;
  top: 0px;
  z-index: -1;
  img {
    width: 100vw;
    height: 100vh;
    @media (max-width: 768px) {
      width: initial;
    }
  }
`;

const ImageTitle = styled.div`
  align-items: flex-end;
  display: flex;
  -webkit-box-pack: start;
  justify-content: flex-start;
  margin: 0px auto;
  height: 5vw;
  min-height: 20px;
  padding-bottom: 24px;
  width: 100%;
  img {
    max-width: 600px;
    min-width: 200px;
    width: 35vw;
  }
`;

const ContentMeta = styled.div`
  max-width: 874px;
`;

const Controls = styled.div`
  align-items: center;
  display: flex;
  flex-flow: row nowrap;
  margin: 2px 0px;
  min-height: 56px;
`;

const Player = styled.button`
  font-size: 15px;
  margin: 0px 22px 0px 0px;
  padding: 0px 24px;
  height: 56px;
  border-radius: 4px;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  letter-spacing: 1.8px;
  text-align: center;
  text-transform: uppercase;
  background: rgb (249, 249, 249);
  border: none;
  color: rgb(0, 0, 0);
  img {
    width: 32px;
  }
  &:hover {
    background: rgb(198, 198, 198);
  }
  @media (max-width: 768px) {
    height: 45px;
    padding: 0px 12px;
    font-size: 12px;
    margin: 0px 10px 0px 0px;
    img {
      width: 25px;
    }
  }
`;

const Trailer = styled(Player)`
  background: rgba(249, 249, 249);
  border: 1px solid rgb(249, 249, 249);
  color: rgb(0, 0, 0, 0.9);
`;

const DeleteFromList = styled.div`
  margin-right: 16px;
  height: 44px;
  width: 44px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(0, 0, 0, 0.6);
  border-radius: 50%;
  border: 2px solid white;
  cursor: pointer;
  span {
    background-color: rgb(249, 249, 249);
    display: inline-block;
    &:first-child {
      height: 2px;
      transform: translate(1px, 0px) rotate(0deg);
      width: 16px;
    }
    &:nth-child(2) {
      height: 16px;
      transform: translateX(-8px) rotate(0deg);
      width: 2px;
    }
  }
`;



const SubTitle = styled.div`
  color: rgb(249, 249, 249);
  font-size: 15px;
  min-height: 20px;
  @media (max-width: 768px) {
    font-size: 12px;
  }
`;

const Description = styled.div`
  line-height: 1.4;
  font-size: 20px;
  padding: 16px 0px;
  color: rgb(249, 249, 249);
  @media (max-width: 768px) {
    font-size: 14px;
  }
`;



export default MaListe;  