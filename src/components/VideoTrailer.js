import React from 'react'
import styled from 'styled-components'
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import db from "../firebase";
import ReactPlayer from 'react-player'
import Lightbox from 'react-images';



  const VideoTrailer = (props) => {
    const { id } = useParams();
    const [detailData, setDetailData] = useState({});
    
  
    useEffect(() => {
      db.collection("movies")
        .doc(id)
        .get()
        .then((doc) => {
          if (doc.exists) {
            setDetailData(doc.data());
          } else {
            console.log("no such document in firebase 🔥");
          }
        })
        .catch((error) => {
          console.log("Error getting document:", error);
        });
    }, [id]);
  
  return (
<MyVideo>
<ReactPlayer id='video' url={detailData.trailer}
      controls
      // playing
      width="100%"
      height="100%" />
 </MyVideo>  )
}


const MyVideo =styled.div`
position: absolute;
top:50px;
bottom:0;
margin-left:35%;
height:80%;
width: 55%;
* @media (max-width: 768px) {
  display: none;
  bottom:0
  position:absolute;
  top: 200px;

} */
`
export default VideoTrailer