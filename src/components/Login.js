import React from 'react'
import styled from 'styled-components'

function Login() {
  return (
    <Container>
      <Content>
        <CTA>
          <CTALogoOne src="images/disney.png"/>
          <SignUp>
            GET ALL THERE
          </SignUp>
          <Description>
          *You can now subscribe to see our latest disney movies, a project realised by Zilio Bruno Using React contact me at : zilio.bruno31@gmail.com if you are interested, thanks you ! have a nice day 
          </Description>
          <CTALogoTwo src="images/cta-logo-two.png"/>

          
        </CTA>
      </Content>
      </Container>
  )
}

export default Login

const Container = styled.div`
min-height: calc(100vh - 70px);
padding: 0 calc(3.5vw+5px);
position:relative;
overflow-x:hidden;
display:flex;
align-items:top;
justify-content:center;
text-align: center;
cursor:grab;


&:before{
    background-position:top;
    background-size:cover;

    background:url("/images/login-background.jpg");
    no-repeat fixed;
    content:"";
    position:absolute;
    top:0;
    left:0;
    right:0;
    bottom:0;
    z-index:-1;
}
`
const Content = styled.div`

`
const CTA = styled.div`
max-width:650px;
padding:80px 40px;
display:flex;
flex-direction:column;
margin-top:100px;
align-items:center;

`
const CTALogoOne = styled.img`

`
const SignUp = styled.a`
width:100%;
background-color: #0063e5;
font-weight:bold;
padding:17px 0;
color:#f9f9f9;
border-radius:4px;
text-align:center;
font-size:18px;
cursor:pointer;
transition: all 250ms;
letter-spacing:1.5px;
margin-top:-70px;
margin-bottom:12px;

&:hover{
  background-color: #0483ee;

}

`
const Description = styled.p`
font-size:12px;
letter-spacing:1.5px
text-align: center;
line-height:1.5;


`
const CTALogoTwo = styled(CTALogoOne)`
width: 90%;
`